<?php
namespace App\Models\MyAWS;

class MyAwsFactory {
    
    /**
     * 
     * @return \App\Models\MyAWS\MyAws
     */
    public static function create() {
        $aws = new MyAws();
        
        $ec2 = \AWS::createClient('ec2');

        // Get All VPC information
        $vpcResult = $ec2->describeVpcs();
        foreach($vpcResult['Vpcs'] as $v) {
            $vpcId = $v['VpcId'];
            $vpcName = '';
            if(array_key_exists('Tags', $v)) {
                foreach($v['Tags'] as $tags) {
                    if($tags['Key'] == 'Name') {
                        $vpcName = $tags['Value'];
                    }
                }
            }
            $vpc = new MyVpc($vpcId, $vpcName);
            $vpc->set('CidrBlock', $v['CidrBlock']);
            $vpc->set('State', $v['State']);
            $aws->addVpc($vpc);
        }

        // Get all AZ information
        $azResult = $ec2->describeAvailabilityZones();
        foreach($azResult['AvailabilityZones'] as $a) {
            $az = new MyAZ($a['ZoneName']);
            $aws->addAZ($az);
        }
        unset($subnetResult);

        // Get all Subnet information
        $subnetResult = $ec2->describeSubnets();

        foreach($subnetResult['Subnets'] as $subnet) {
            $snName = '';
            if(array_key_exists('Tags', $subnet)) {
                foreach($subnet['Tags'] as $tags) {
                    if($tags['Key'] == 'Name') {
                        $snName = $tags['Value'];
                    }
                }
            }
            
            $sn = new MySubnet($subnet['SubnetId'], $snName);
            $sn->set('CidrBlock', $subnet['CidrBlock']);
            $sn->set('State', $subnet['State']);
            $sn->set('AvailableIpAddressCount', $subnet['AvailableIpAddressCount']);

            $vpc = $aws->getVpc($subnet['VpcId']);
            $vpc->addSubnet($sn);
            $az = $aws->getAZ($subnet['AvailabilityZone']);
            $az->addSubnet($sn);
        }
        unset($subnetResult);

        // Get all Route Table information
        $rtResults = $ec2->describeRouteTables();

        foreach($rtResults['RouteTables'] as $rtResult) {
            $rt = new MyRouteTable($rtResult['RouteTableId']);

            $vpc = $aws->getVpc($rtResult['VpcId']);
            $rt->setVpc($vpc);
            
            foreach($rtResult['Routes'] as $routeResult) {
                $r = new MyRoute($routeResult['DestinationCidrBlock']);
                $r->setVpc($vpc);
                $r->setRouteTable($rt);
                $r->setFromArrayWithName('DestinationCidrBlock', $routeResult);
                if(array_key_exists('VpcPeeringConnectionId', $routeResult)) {
                    $r->setRouteType('VPC Peering');
                    $r->set('EachTypeId', $routeResult['VpcPeeringConnectionId']);
                }
                if(array_key_exists('GatewayId', $routeResult)) {
                    $r->setRouteType('Normal');
                    $r->set('EachTypeId', $routeResult['GatewayId']);
                }
                if(array_key_exists('NatGatewayId', $routeResult)) {
                    $r->setRouteType('NAT Gateway');
                    $r->set('EachTypeId', $routeResult['NatGatewayId']);
                }
                $r->setFromArrayWithName('State', $routeResult);
                $r->setFromArrayWithName('Origin', $routeResult);
                $rt->addRoute($r);
            }
            
            foreach($rtResult['Associations'] as $asResult) {
                if(array_key_exists('SubnetId', $asResult)) {
                    $rt->addAssociatedSubnet($vpc->getSubnet($asResult['SubnetId']));
                }
                if($asResult['Main'] == 'true') {
                    $rt->setMain(true);
                }
            }
        }
        unset($routeResult);

        // Get Security Group information
        $sgsResult = $ec2->describeSecurityGroups();
        foreach($sgsResult['SecurityGroups'] as $sgResult) {
            // メモリ浪費を防ぐために必要な情報だけコピー
            // $this->copyArrayParts($sgResult, $sg, [ 'GroupId', 'GroupName', 'VpcId', 'Description', 'IpPermissions', 'IpPermissionsEgress']);
            $sg = new MySecurityGroup($sgResult['GroupId'], $sgResult['GroupName']);
            $sg->setFromArrayWithName('Description', $sgResult);
            $sg->setFromArrayWithName('IpPermissions', $sgResult);
            $sg->setFromArrayWithName('IpPermissionsEgress', $sgResult);
            
            $vpc = $aws->getVpc($sgResult['VpcId']);
            $vpc->addSecurityGroup($sg);
        }

        unset($sgsResult);

        // Get NAT Gateway information
        $ngwResults = $ec2->describeNatGateways();
        foreach($ngwResults['NatGateways'] as $ngwResult) {
            $ngw = new MyNatGateway($ngwResult['NatGatewayId']);
            $ngw->set('PublicIpAddress', $ngwResult['NatGatewayAddresses'][0]['PublicIp']);
            $ngw->set('PrivateIpAddress', $ngwResult['NatGatewayAddresses'][0]['PrivateIp']);
            
            $sn = $aws->getSubnet($ngwResult['SubnetId']);
            $sn->addNatGateway($ngw);
        }

        unset($ngwResults);

        // Get Internet Gateway information
        $igwResults = $ec2->describeInternetGateways();
        foreach($igwResults['InternetGateways'] as $igwResult) {
            $igw = new MyInternetGateway($igwResult['InternetGatewayId']);
            
            foreach($igwResult['Attachments'] as $at) {
                $vpc = $aws->getVpc($at['VpcId']);
                $vpc->addInternetGateway($igw);
            }
        }

        unset($igwResults);

        // Get Instance information
        $ec2sResult = $ec2->describeInstances();
        foreach($ec2sResult['Reservations'] as &$res) {
            foreach($res['Instances'] as &$ins) {
                // Get instance name from Tags
                $instanceName = '';
                if(array_key_exists('Tags', $ins)) {
                    foreach($ins['Tags'] as &$tags) {
                        if($tags['Key'] == 'Name') {
                            $instanceName = $tags['Value'];
                        }
                    }
                }

                $instance = new MyInstance($ins['InstanceId'], $instanceName);
                $instance->set('AvailabilityZone', $ins['Placement']['AvailabilityZone']);
                $instance->set('State', $ins['State']['Name']);
                $stateCSSClass = "icon-green";
                if($ins['State']['Name'] == "stopped") {
                    $stateCSSClass = "icon-red";
                }
                $instance->set('StateCSSClass', $stateCSSClass);
                $instance->setFromArrayWithName('PrivateIpAddress', $ins);
                $instance->setFromArrayWithName('PublicIpAddress', $ins);
                $instance->setFromArrayWithName('InstanceType', $ins);
                $instance->setFromArrayWithName('RootDeviceType', $ins);
                $instance->setFromArrayWithName('RootDeviceName', $ins);
                if(array_key_exists('SubnetId', $ins)) {
                    // terminated の場合は Subnet が無い
                    $aws->getVpc($ins['VpcId'])->getSubnet($ins['SubnetId'])->addInstance($instance);
                }
                foreach($ins['SecurityGroups'] as $sg) {
                    $instance->addSecurityGroup($aws->getSecurityGroup($sg['GroupId']));
                }
            }
        }
        unset($ec2sResult);

        // Get ELB information
        $elbClient = \AWS::createClient('elasticloadbalancing');
        $elbsResult = $elbClient->describeLoadBalancers();
        $allElbs = $elbsResult['LoadBalancerDescriptions'];

        foreach($allElbs as $elbResult) {
            $elb = new MyLoadBalancer($elbResult['LoadBalancerName'], $elbResult['LoadBalancerName']);
            foreach($elbResult['Subnets'] as $sbId) {
                $elb->addSubnet($aws->getSubnet($sbId));
                $aws->getSubnet($sbId)->addLoadBalancer($elb);
            }
            foreach($elbResult['SecurityGroups'] as $sgId) {
                $elb->addSecurityGroup($aws->getSecurityGroup($sgId));
            }
            foreach($elbResult['Instances'] as $insResult) {
                foreach($insResult as $name => $val) {
                    $ins = $elb->getVpc()->getInstance($val);
                    if($ins) {
                        $elb->addInstance($aws->getInstance($val));
                    }
                }
            }
        }

        // Get RDS information
        $rds = \AWS::createClient('rds');
        $rdssResult = $rds->describeDBInstances();
        $allDBs = $rdssResult['DBInstances'];
        $dbs = array();

        foreach($allDBs as $dbResult) {
            $db = new MyRDS($dbResult['DBInstanceIdentifier'], $dbResult['DBInstanceIdentifier']);
            $db->set('AvailabilityZone', $dbResult['AvailabilityZone']);
            $vpc = $aws->getVpc($dbResult['DBSubnetGroup']['VpcId']);
            $db->setVpc($vpc);
            foreach($dbResult['DBSubnetGroup']['Subnets'] as $sn) {
                $db->addSubnet($vpc->getSubnet($sn['SubnetIdentifier']));
            }

            foreach($dbResult['VpcSecurityGroups'] as $sg) {
                $db->addSecurityGroup($vpc->getSecurityGroup($sg['VpcSecurityGroupId']));
            }
            $db->set('DBInstanceClass', $dbResult['DBInstanceClass']);
            $db->set('StorageType', $dbResult['StorageType']);
            $db->set('EngineVersion', $dbResult['EngineVersion']);
            $db->set('DBInstanceStatus', $dbResult['DBInstanceStatus']);
            $db->set('EndpointAddress', $dbResult['Endpoint']['Address']);
            $db->set('EndpointPort', $dbResult['Endpoint']['Port']);
            $db->set('State', $ins['DBInstanceStatus']);
            $db->set('StateCSSClass', ($dbResult['DBInstanceStatus'] == "available") ? "icon-green" : "icon-red");
        }

        $aws->asort();
        return $aws;
    }
    
}
