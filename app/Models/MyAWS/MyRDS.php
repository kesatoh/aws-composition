<?php
namespace App\Models\MyAWS;

use App\Models\MyAWS\MyAwsObject;

class MyRDS extends MyAwsObject {
    var $vpc;
    var $subnets;
    var $id;
    var $securityGroups = array();
    
    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
    }
    
    public function setVpc($vpc) {
        $vpc->addRDS($this);
        $this->vpc = $vpc;
    }

    public function getVpc() {
        return $this->vpc;
    }

    public function addSubnet(MySubnet $sn) {
        $sn->addRDS($this);
        $this->subnets[$sn->getId()] = $sn;
    }

    public function getSubnet($id) {
        return $this->subnets[$id];
    }

    public function getSubnets() {
        return $this->subnets;
    }

    public function addSecurityGroup(MySecurityGroup $sg) {
        $sg->addRDS($this);
        $this->securityGroups[$sg->getId()] = $sg;
    }
    
    public function getSecurityGroups() {
        return $this->securityGroups;
    }

    public function getId() {
        return $this->id;
    }
}