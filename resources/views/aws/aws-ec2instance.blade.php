<div id="ec2_instance" class="tab-pane fade">
    @foreach($aws->getInstances() as $instance)
    <div class="panel panel-default">
        <div id="{{ $instance->getId() }}" class="panel-heading" href="#{{ $instance->getId() }}-body" data-toggle="collapse">
            <span class="label label-primary">{{ $instance->getId() }}</span> <strong>"{{ $instance->getName() }}"</strong> <span class="{{ $instance->get('StateCSSClass') }}">{{ $instance->get('State') }}</span>
        </div>

        <div id="{{ $instance->getId() }}-body" class="panel-body panel-collapse collapse">

            <div class="col-md-6">
                <table class="table table-bordered table-condensed">
                    </tr>
                        <th class="col-md-2 active">AZ</th>
                        <td class="col-md-10 text-nowrap" colspan="2">{{ $instance->get('AvailabilityZone') }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2 active">VPC</th>
                        <td class="col-md-8 text-nowrap"><span class="label label-danger">{{ $instance->getVpc()->getId() }}</span> "{{ $instance->getVpc()->getName() }}"</td>
                        <td class="col-md-2 text-nowrap">{{ @$instance->getVpc()->get('CidrBlock') }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2 active">Subnet</th>
                        <td class="col-md-8 text-nowrap"><span class="label label-warning">{{ $instance->getSubnet()->getId() }}</span> "{{ $instance->getSubnet()->getName() }}"</td>
                        <td class="col-md-2 text-nowrap">{{ $instance->getSubnet()->get('CidrBlock') }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2 active">Private IP</th>
                        <td class="text-nowrap text-right" colspan="2">{{ $instance->get('PrivateIpAddress') }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2 active">Public IP</th>
                        <td class="text-nowrap text-right" colspan="2">{{ $instance->get('PublicIpAddress') }}</td>
                    </tr>
                </table>
            </div>

            <div class="col-md-6">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th class="col-md-2 text-nowrap active">Instance Type</th>
                        <td class="col-md-10 text-nowrap">{{ @$instance->get('InstanceType') }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2 text-nowrap active">Root Device Type</th>
                        <td class="col-md-10 text-nowrap">{{ $instance->get('RootDeviceType') }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2 text-nowrap active">Root Device Name</th>
                        <td class="col-md-10 text-nowrap">{{ $instance->get('RootDeviceName') }}</td>
                    </tr>
                </table>
            </div>

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Security Group
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">

                        @foreach($instance->getSecurityGroups() as $sg)
                                <p>
                                    <a href="#{{ $sg->getId() }}">
                                    <span class="label label-info">{{ $sg->getId() }}</span>
                                    {{ $sg->getName() }}
                                    </span>
                                    </a>
                                </p>

                            @if($sg)
                                @include('aws.sg.aws-sgdetail')
                            @endif

                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
