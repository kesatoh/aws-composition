<div id="rds_database" class="tab-pane fade">
    @foreach($aws->getRDSs() as $db)
    <div class="panel panel-default">
        <div id="{{ $db->getId() }}" class="panel-heading" href="#{{ $db->getId() }}-body" data-toggle="collapse">
            <span class="label label-primary">{{ $db->getId() }}</span> <strong>"{{ $db->getName() }}"</strong> <span class="{{ $db->get('StateCSSClass') }}">{{ $db->get('State') }}</span>
        </div>

        <div id="{{ $db->getId() }}-body" class="panel-body panel-collapse collapse">
            <div class="col-md-5">
                <table class="table table-bordered table-condensed">
                    </tr>
                        <th class="col-md-2 active">AZ</th>
                        <td class="col-md-10 text-nowrap" colspan="2">{{ $db->get('AvailabilityZone') }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2 active">VPC</th>
                        <td class="col-md-8 text-nowrap"><span class="label label-danger">{{ $db->getVpc()->getId() }}</span> "{{ $db->getVpc()->getName() }}"</td>
                        <td class="col-md-2 text-nowrap">{{ @$db->getVpc()->get('CidrBlock') }}</td>
                    </tr>
                    @foreach($db->getSubnets() as $snId => $sn)
                    <tr>
                        <th class="col-md-2 active">Subnet</th>
                        <td class="col-md-8 text-nowrap">
                            <span class="label label-warning">{{ $snId }}</span> "{{ $sn->getName() }}"
                        </td>
                        <td class="col-md-2 text-nowrap">
                            {{ $sn->get('CidrBlock') }}
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>

            <div class="col-md-7">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th class="col-md-2 text-nowrap active">Endpoint Address</th>
                        <td class="col-md-10 text-nowrap">{{ $db->get('EndpointAddress') }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2 text-nowrap active">Endpoint Port</th>
                        <td class="col-md-10 text-nowrap">{{ $db->get('EndpointPort') }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2 text-nowrap active">DB Instance Class</th>
                        <td class="col-md-10 text-nowrap">{{ @$db->get('DBInstanceClass') }}</td>
                    </tr>
                    <tr>
                        <th class="col-md-2 text-nowrap active">Storage Type</th>
                        <td class="col-md-10 text-nowrap">{{ $db->get('StorageType') }} ({{ $db->get('EngineVersion') }})</td>
                    </tr>
                </table>
            </div>

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        VPC Security Group
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">

                        @foreach($db->getSecurityGroups() as $sg)
                                <p>
                                    <a href="#{{ $sg->getId() }}">
                                    <span class="label label-info">{{ $sg->getId() }}</span>
                                    {{ $sg->getName() }}
                                    </span>
                                    </a>
                                </p>

                            @if($sg)
                                @include('aws.sg.aws-sgdetail')
                            @endif

                        @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @endforeach
</div>