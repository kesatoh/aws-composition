<?php
namespace App\Models\MyAWS;

use App\Models\MyAWS\MyAwsObject;

class MyVpc extends MyAwsObject {
    var $aws;
    var $azs;
    var $subnets;
    var $igws;
    var $mainRouteTable;
    var $securityGroups;
    var $instances;
    var $elbs;
    var $rdss;
    
    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
        $this->azs = array();
        $this->subnets = array();
        $this->instances = array();
    }
    
    public function setAws(MyAws $aws) {
        $this->aws = $aws;
    }
    
    public function addSubnet(MySubnet $subnet) {
        $subnet->setVpc($this);
        $this->subnets[$subnet->getId()] = $subnet;
    }
    
    public function getSubnet($id) {
        return $this->subnets[$id];
    }

    public function getSubnets() {
        return $this->subnets;
    }

    public function addInternetGateway(MyInternetGateway $igw) {
        $this->igws[$igw->getId()] = $igw;
    }
    
    public function getInternetGateway($id) {
        return $this->igws[$id];
    }

    public function getInternetGateways() {
        return $this->igws;
    }

    public function setMainRouteTable(MyRouteTable $rt) {
        $this->mainRouteTable = $rt;
    }
    
    public function getMainRouteTable() {
        return $this->mainRouteTable;
    }

    public function addSecurityGroup(MySecurityGroup $sg) {
        $sg->setVpc($this);
        $this->securityGroups[$sg->getId()] = $sg;
    }
    
    public function getSecurityGroup($id) {
        return $this->securityGroups[$id];
    }
    
    public function getSecurityGroups() {
        return $this->securityGroups;
    }
    
    public function addInstance(MyInstance $instance) {
        if(array_key_exists($instance->getId(), $this->instances)) {
            $this->instances[$instance->getId()] = $instance;
            $instance->setVpc($this);
        }
    }
    
    public function getInstance($id) {
        return @$this->instances[$id];
    }
    
    public function getInstances() {
        return $this->instances;
    }
    
    public function addLoadBalancer(MyLoadBalancer $elb) {
        $elb->setVpc($this);
        $this->elbs[$elb->getId()] = $elb;
    }
    
    public function getLoadBalancer($id) {
        return $this->elbs[$id];
    }
    
    public function getLoadBalancers() {
        return $this->elbs;
    }
    
    public function addRDS(MyRDS $rds) {
        $this->rdss[$rds->getId()] = $rds;
    }
    
    public function getRDS($id) {
        return $this->rdss[$id];
    }
    
    public function getRDSs() {
        return $this->rdss;
    }
    
    public function asort() {
        foreach($this->subnets as $sn) {
            $sn->asort();
        }
        if(count($this->subnets) > 0) {
            $sort = array();
            foreach($this->subnets as $id => $obj) {
                $sort[$id] = $obj->getName();
            }
            array_multisort($sort, SORT_ASC, $this->subnets);
        }
    }
    
}