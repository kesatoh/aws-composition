<?php
namespace App\Models\MyAWS;

use App\Models\MyAWS\MyAwsObject;

class MyAZ extends MyAwsObject {
    var $vpc;
    var $subnets;
    
    public function __construct($id) {
        $this->id = $id;
        $this->subnets = array();
        $this->instances = array();
    }
    
    public function setVpc($vpc) {
        $this->vpc = $vpc;
    }
    
    public function getVpc() {
        return $this->vpc;
    }

    public function addSubnet(MySubnet $subnet) {
        $subnet->setAZ($this);
        $this->subnets[$subnet->getId()] = $subnet;
    }
    
    public function getSubnet($id) {
        return $this->subnets[$id];
    }

    public function getSubnets() {
        return $this->subnets;
    }
/*
    public function addInstance(MyInstance $ins) {
        $this->instances[$ins->getId()] = $ins;
        $this->vpc->addInstance($ins);
    }
*/
    public function getInstance($id) {
        return $this->instances[$id];
    }
    
    public function getInstances() {
        return $this->instances;
    }

    public function addLoadBalancer(MyLoadBalancer $elb) {
        $this->elbs[$elb->getId()] = $elb;
        $this->vpc->addLoadBalancer($elb);
    }
    
    public function getLoadBalancer($id) {
        return $this->elbs[$id];
    }
    
    public function getLoadBalancers() {
        return $this->elbs;
    }
    
}