<div id="vpc" class="tab-pane fade in active">
    <table class="table table-bordered">
        <tr class="active">
            <th>VPC ID</td>
            <th>VPC Name</td>
            <th>CIDR Block</td>
            <th>State</td>
        </tr>
    @foreach($aws->getVpcs() as $vpc)
        <tr>
            <td class="text-nowrap"><span class="label label-danger">{{ $vpc->getId() }}</span></td>
            <td class="text-nowrap"><strong>{{ $vpc->getName('VpcName') }}</strong></td>
            <td class="text-nowrap">{{ $vpc->get('CidrBlock') }}</td>
            <td class="text-nowrap text-center @if ($vpc->get('State')=='available') bg-success @endif">{{ $vpc->get('State') }}</td>
        </tr>
    @endforeach
    </table>
</div>

