<?php
namespace App\Models\MyAWS;

use App\Models\MyAWS\MyAwsObject;

class MySubnet extends MyAwsObject {
    var $vpc;
    var $az;
    var $routeTable;
    var $natGateways;
    var $instances;
    var $elbs;
    var $rdss;
    
    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
        $this->instances = array();
        $this->elbs = array();
        $this->natGateways = array();
    }
    
    public function setVpc($vpc) {
        $this->vpc = $vpc;
    }
    
    public function getVpc() {
        return $this->vpc;
    }

    public function setAZ($az) {
        $this->az = $az;
    }
    
    public function getAZ() {
        return $this->az;
    }

    public function setRouteTable($rt) {
        $this->routeTable = $rt;
    }
    
    public function getRouteTable() {
        return $this->routeTable;
    }

    public function addNatGateway(MyNatGateway $ngw) {
        if(!array_key_exists($ngw->getId(), $this->natGateways)) {
            $this->natGateways[$ngw->getId()] = $ngw;
            $ngw->setSubnet($this);
        }
    }
    
    public function getNatGateway($id) {
        return @$this->natGateways[$id];
    }
    
    public function getNatGateways() {
        return $this->natGateways;
    }

    public function addInstance(MyInstance $ins) {
        if(!array_key_exists($ins->getId(), $this->instances)) {
            $this->instances[$ins->getId()] = $ins;
            $ins->setSubnet($this);
        }
        $ins->setVpc($this->getVpc());
    }
    
    public function getInstance($id) {
        $result = null;
        foreach($this->subnets as $sn) {
            $result = $sn->getInstance($id);
            if($result != null) {
                break;
            }
        }
        return $result;
    }
    
    public function getInstances() {
        return $this->instances;
    }

    public function addLoadBalancer(MyLoadBalancer $elb) {
        $this->elbs[$elb->getId()] = $elb;
        $this->vpc->addLoadBalancer($elb);
    }
    
    public function getLoadBalancer($id) {
        return $this->elbs[$id];
    }
    
    public function getLoadBalancers() {
        return $this->elbs;
    }
    
    public function addRDS(MyRDS $rds) {
        $this->rdss[$rds->getId()] = $rds;
        $this->vpc->addRDS($rds);
    }
    
    public function getRDS($id) {
        return $this->rdss[$id];
    }
    
    public function getRDSs() {
        return $this->rdss;
    }
    
    public function asort() {
        $sort = array();
        foreach($this->instances as $id => $obj) {
            $sort[$id] = $obj->getName();
        }
        array_multisort($sort, SORT_ASC, $this->instances);
    }
}