<?php
namespace App\Models\MyAWS;

abstract class MyAwsObject {
    var $id;
    var $name;
    var $vars = array();
    
    public function getId() {
        return $this->id;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function set($name, $val) {
        $this->vars[$name] = $val;
    }
    
    public function setFromArrayWithName($name, $arr) {
        $val = '';
        $result = false;
        if(array_key_exists($name, $arr)) {
            $val = $arr[$name];
            $result = true;
        }
        $this->vars[$name] = $val;
        
        return $result;
    }
    
    public function get($name) {
        $result = '';
        if(array_key_exists($name, $this->vars)) {
            $result = $this->vars[$name];
        }
        return $result;
    }
    
}