<?php
namespace App\Models\MyAWS;

use App\Models\MyAWS\MyAwsObject;

class MyInstance extends MyAwsObject {
    var $vpc;
    var $subnet;
    var $securityGroups = array();
    
    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
    }
    
    public function setVpc(MyVpc $vpc) {
        if($this->vpc = $vpc) {
            $this->vpc = $vpc;
        }
        $vpc->addInstance($this);
    }

    public function getVpc() {
        return $this->vpc;
    }

    public function setSubnet(MySubnet $sn) {
        $sn->addInstance($this);
        $this->subnet = $sn;
    }

    public function getSubnet() {
        return $this->subnet;
    }

    public function addSecurityGroup(MySecurityGroup $sg) {
        $sg->addInstance($this);
        $this->securityGroups[$sg->getId()] = $sg;
    }
    
    public function getSecurityGroups() {
        return $this->securityGroups;
    }

}