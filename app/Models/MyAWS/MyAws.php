<?php
namespace App\Models\MyAWS;

class MyAws {
    var $vpcs;
    var $azs;
    
    public function __construct() {
        $this->vpcs = array();
    }
    
    public function addVpc(MyVpc $vpc) {
        $vpc->setAws($this);
        $this->vpcs[$vpc->getId()] = $vpc;
    }
    
    public function getVpcs() {
        return $this->vpcs;
    }
    
    public function getVpc($id) {
        return $this->vpcs[$id];
    }
    
    public function getSubnet($pid) {
        $result = null;
        foreach($this->vpcs as $vpc) {
            foreach($vpc->getSubnets() as $id => $sn) {
                if($id == $pid) {
                    $result = $sn;
                    break;
                }
            }
        }
        return $result;
    }

    public function getSubnets() {
        $subnets = array();
        foreach($this->vpcs as $vpc) {
            foreach($vpc->getSubnets() as $id => $subnet) {
                $subnets[$id] = $subnet;
            }
        }
        return $subnets;
    }
    
    public function addAZ(MyAZ $az) {
        $az->setVpc($this);
        $this->azs[$az->getId()] = $az;
    }
    
    public function getAZ($id) {
        return $this->azs[$id];
    }

    public function getAZs() {
        return $this->azs;
    }
    
    public function getSecurityGroups() {
        $sgs = array();
        foreach($this->vpcs as $vpc) {
            foreach($vpc->getSecurityGroups() as $id => $sg) {
                $sgs[$id] = $sg;
            }
        }
        return $sgs;
    }
    
    public function getSecurityGroup($pid) {
        $result = null;
        foreach($this->vpcs as $vpc) {
            foreach($vpc->getSecurityGroups() as $id => $sg) {
                if($id == $pid) {
                    $result = $sg;
                    break;
                }
            }
        }
        return $result;
    }
    
    public function getInstances() {
        $ins = array();
        foreach($this->vpcs as $vpc) {
            foreach($vpc->subnets as $subnet) {
                foreach($subnet->getInstances() as $id => $in) {
                    $ins[$id] = $in;
                }
            }
        }
        return $ins;
    }
    
    public function getInstance($id) {
        $result = null;
        foreach($this->vpcs as $vpc) {
            $result = $vpc->getInstance($id);
            if($result != null) {
                break;
            }
        }
        return $result;
    }
    
    public function getLoadBalancers() {
        $ins = array();
        foreach($this->vpcs as $vpc) {
            foreach($vpc->subnets as $subnet) {
                foreach($subnet->getLoadBalancers() as $id => $in) {
                    $ins[$id] = $in;
                }
            }
        }
        return $ins;
    }
    
    public function getRDSs() {
        $rdss = array();
        foreach($this->vpcs as $vpc) {
            if(count($vpc->getRDSs()) > 0) {
                foreach($vpc->getRDSs() as $id => $rds) {
                    $rdss[$id] = $rds;
                }
            }
        }
        return $rdss;
    }

    public function asort() {
        foreach($this->vpcs as $vpc) {
            $vpc->asort();
        }
        if(count($this->vpcs) > 0) {
            $sort = array();
            foreach($this->vpcs as $id => $obj) {
                $sort[$id] = $obj->getName();
            }
            array_multisort($sort, SORT_ASC, $this->vpcs);
        }
    }
}
