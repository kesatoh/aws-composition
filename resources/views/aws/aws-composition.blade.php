<script type="text/javascript">
$(function() {
});
</script>
<div id="composition" class="tab-pane fade in active">
    <div>
        <fieldset>
            <legend><img src="aws/img/General_AWScloud.png"></legend>
            @foreach($aws->getVpcs() as $vpcId => $vpc)
            <fieldset>
                <legend><img src="aws/img/General_virtualprivatecloud.png" width="32"> <span class="label label-danger">{{ $vpc->getId() }}</span> {{ $vpc->getName() }} ({{ $vpc->get('CidrBlock') }})</legend>

                <div class="panel panel-default">
                    <div id="{{ $vpc->getId() }}" class="panel-heading" href="#{{ $vpc->getId() }}-networking-body" data-toggle="collapse">VPC Default Networking Info
                    </div>
                    <div id="{{ $vpc->getId() }}-networking-body" class="panel-body panel-collapse collapse">
                        <table class="table table-bordered">
                            <tr class="active">
                                <th>Destination CIDR Block</th>
                                <th>Type</th>
                                <th>GatewayId</th>
                                <th>State</th>
                                <th>Origin</th>
                            </tr>
                            @foreach($vpc->getMainRouteTable()->getRoutes() as $route)
                            <tr>
                                <td>{{ $route->getId() }}</td>
                                <td>{{ $route->getRouteType() }}</td>
                                <td>{{ $route->get('EachTypeId') }}</td>
                                <td>{{ $route->get('State') }}</td>
                                <td>{{ $route->get('Origin') }}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

                <div class="col-md-10">
                    @foreach($vpc->getSubnets() as $subnetId => $subnet)
                    
                    <fieldset>
                        <legend><span class="label label-warning">{{ $subnet->getId() }}</span> {{ $subnet->getName() }} ({{ $subnet->get('CidrBlock') }})</legend>

                        @if($subnet->getRouteTable())
                        <div class="panel panel-default">
                            <div id="{{ $subnet->getId() }}" class="panel-heading" href="#{{ $subnet->getId() }}-networking-body" data-toggle="collapse">Subnet Specific Networking Info
                            </div>
                            <div id="{{ $subnet->getId() }}-networking-body" class="panel-body panel-collapse collapse">
                                <table class="table table-bordered">
                                    <tr class="active">
                                        <th>Destination CIDR Block</th>
                                        <th>Type</th>
                                        <th>GatewayId</th>
                                        <th>State</th>
                                        <th>Origin</th>
                                    </tr>
                                        @foreach($subnet->getRouteTable()->getRoutes() as $route)
                                        <tr>
                                            <td>{{ $route->getId() }}</td>
                                            <td>{{ $route->getRouteType() }}</td>
                                            <td>{{ $route->get('EachTypeId') }}</td>
                                            <td>{{ $route->get('State') }}</td>
                                            <td>{{ $route->get('Origin') }}</td>
                                        </tr>
                                        @endforeach
                                </table>
                            </div>
                        </div>
                        @endif
                    
                        @if(count($subnet->getNatGateways()) > 0)
                        <div class="col-md-12">
                            <ul class="list-inline">
                            @foreach($subnet->getNatGateways() as $ngwId => $ngw)
                            <li class="text-center col-md-2">
                                <div id="{{ $ngwId }}" class="icon-overlay">
                                    <img src="aws/img/Compute_AmazonVPC_VPCNATgateway.png" height="56">
                                </div>
                                <p class="ellipse">{{ $ngwId }}<br>
                                {{ $ngw->get('PublicIpAddress') }}<br>
                                {{ $ngw->get('PrivateIpAddress') }}</p>
                            </li>
                            @endforeach
                            </ul>
                        </div>
                        @endif

                        @if(count($subnet->getInstances()) > 0)
                        <div class="col-md-12">
                            <ul class="list-inline">
                            @foreach($subnet->getInstances() as $instanceId => $instance)
                            <li class="text-center col-md-2">
                                <div id="{{ $instanceId }}" class="icon-overlay">
                                    <img src="aws/img/Compute_AmazonEC2_instance.png" height="56">
                                    @if(@$instance->get('StateCSSClass'))
                                    <div height="32" class="{{ $instance->get('StateCSSClass') }}-overlay"></div>
                                    @endif
                                </div>
                                <p class="ellipse">{{ $instance->getName() }}<br>{{ $instance->get('PrivateIpAddress') }}</p>
                            </li>
                            @endforeach
                            </ul>
                        </div>
                        @endif

                        @if(count($subnet->getRDSs()) > 0)
                        <div class="col-md-12">
                            <ul class="list-inline">
                            @foreach($subnet->getRDSs() as $rdsId => $rds)
                            <li class="text-center col-md-2">
                                <div id="{{ $rdsId }}" class="icon-overlay">
                                    <img src="aws/img/Database_AmazonRDS.png" height="56" title="{{ $rds->getName() }}">
                                    @if(@$rds->get('StateCSSClass'))
                                    <div height="32" class="{{ $rds->get('StateCSSClass') }}-overlay"></div>
                                    @endif
                                </div>
                                <p class="ellipse">{{ $rds->getName() }}</p>
                            </li>
                            @endforeach
                            </ul>
                        </div>
                        @endif
                    </fieldset>
                    @endforeach
                </div>
                
                <div class="col-md-2">
                    <fieldset>
                        <legend>IGW</legend>
                        @if(count($vpc->getInternetGateways()) > 0)
                        <ul>
                            @foreach($vpc->getInternetGateways() as $igwId => $igw)
                            <li class="text-center">
                                <img src="aws/img/Compute_AmazonVPC_Internetgateway.png" height="56">
                                <p class="ellipse">{{ $igw->getId() }}</p>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </fieldset>

                    <fieldset>
                        <legend>ELB</legend>
                        @if(count($vpc->getLoadBalancers()) > 0)
                        <ul>
                            @foreach($vpc->getLoadBalancers() as $balancerId => $elb)
                            <li class="text-center">
                                <div instances="{{ implode(',', $elb->getInstances()) }}">
                                    <img src="aws/img/Compute_ElasticLoadBalancing_ApplicationLoadBalancer.png" height="56">
                                </div>
                                <p class="ellipse">{{ $elb->getName() }}</p>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </fieldset>
                </div>

            </fieldset>
            @endforeach
        </fieldset>
    </div>
</div>