<?php
namespace App\Models\MyAWS;

use App\Models\MyAWS\MyAwsObject;

class MySecurityGroup extends MyAwsObject {
    var $vpc;
    var $instances = array();
    var $elbs = array();
    var $rdss = array();
    
    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
    }
    
    public function setVpc($vpc) {
        $this->vpc = $vpc;
    }

    public function getVpc() {
        return $this->vpc;
    }

    public function addInstance(MyInstance $ins) {
        $this->instances[$ins->getId()] = $ins;
    }
    
    public function getInstances() {
        return $this->instances;
    }
        
    public function addLoadBalancer(MyLoadBalancer $elb) {
        $this->elbs[$elb->getId()] = $elb;
    }
    
    public function getLoadBalancers() {
        return $this->elbs;
    }
        
    public function addRDS(MyRDS $rds) {
        $this->rdss[$rds->getId()] = $rds;
    }
    
    public function getRDSs() {
        return $this->rdss;
    }
        
}