<?php
namespace App\Models\MyAWS;

use App\Models\MyAWS\MyAwsObject;

class MyLoadBalancer extends MyAwsObject {
    var $vpc;
    var $subnets = array();
    var $securityGroup = array();
    var $instances = array();
    
    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
    }
    
    public function getVpc() {
        return $this->vpc;
    }
    
    public function setVpc($vpc) {
        $this->vpc = $vpc;
    }

    public function addSubnet(MySubnet $sn) {
        $sn->addLoadBalancer($this);
        $this->subnet[$sn->getid()] = $sn;
    }

    public function getSubnets() {
        return $this->subnets;
    }

    public function addSecurityGroup(MySecurityGroup $sg) {
        $sg->addLoadBalancer($this);
        $this->securityGroups[$sg->getId()] = $sg;
    }
    
    public function getInstances() {
        return $this->instances;
    }

    public function addInstance(MyInstance $ins) {
        $this->instances[$ins->getId()] = $ins;
    }
    
    public function getSecurityGroups() {
        return $this->securityGroups;
    }

}