<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    echo date('Y/m/d H:i:s');
    return view('welcome');
});

Route::get('/listAll', 'AwsController@listAll');
Route::get('/ddvpc', 'AwsController@ddvpc');
Route::get('/ddaz', 'AwsController@ddaz');
Route::get('/ddsg', 'AwsController@ddsg');
Route::get('/ddec2', 'AwsController@ddec2');
Route::get('/ddrds', 'AwsController@ddrds');
Route::get('/ddsn', 'AwsController@ddsn');
Route::get('/ddelb', 'AwsController@ddelb');
Route::get('/ddebtapp', 'AwsController@ddebtapp');
Route::get('/ddebtenv', 'AwsController@ddebtenv');
Route::get('/ddrt', 'AwsController@ddrt');
Route::get('/ddnatgw', 'AwsController@ddnatgw');
Route::get('/ddigw', 'AwsController@ddigw');
Route::get('/ddeni', 'AwsController@ddeni');
