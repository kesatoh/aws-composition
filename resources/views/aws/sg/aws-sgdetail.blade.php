<table class="table table-bordered table-condensed">
    <tr class="active">
        <th class="col-md-1">In/Out</th>
        <th class="col-md-1">Protocol</th>
        <th class="col-md-1">Port</th>
        <th class="col-md-9">CIDR/SG</th>
    </tr>
    @foreach($sg->get('IpPermissions') as $perm)
    @include('aws.sg.aws-ippermissions', ['IpPermissionName' => 'In', 'icon' => 'glyphicon glyphicon-chevron-right'])
    @endforeach
    @foreach($sg->get('IpPermissionsEgress') as $perm)
    @include('aws.sg.aws-ippermissions', ['IpPermissionName' => 'Out', 'icon' => 'glyphicon glyphicon-chevron-left'])
    @endforeach
</table>
