<div id="subnet" class="tab-pane fade">
    <table class="table table-bordered">
        <tr class="active">
            <th>Subnet ID</th>
            <th>Subnet Name</th>
            <th>VPC</th>
            <th>CIDR Block</th>
            <th>Available IP Count</th>
            <th>State</th>
        </tr>
        @foreach($aws->getSubnets() as $subnetId => $subnet)
        <tr>
            <td class="text-nowrap">{{ $subnetId }}</td>
            <td class="text-nowrap"><strong>{{ $subnet->getName() }}</strong></td>
            <td class="text-nowrap"><span class="label label-danger">{{ $subnet->getVpc()->getId() }}</span> {{ $subnet->getVpc()->getName() }}</td>
            <td class="text-nowrap">{{ $subnet->get('CidrBlock') }}</td>
            <td class="text-nowrap text-right">{{ number_format($subnet->get('AvailableIpAddressCount')) }}</td>
            <td class="text-nowrap text-center @if($subnet->get('State')=='available') bg-success @endif">{{ $subnet->get('State') }}</td>
        </tr>
        @endforeach
    </table>
</div>