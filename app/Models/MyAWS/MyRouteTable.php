<?php
namespace App\Models\MyAWS;

use App\Models\MyAWS\MyAwsObject;

class MyRouteTable extends MyAwsObject {
    var $vpc;
    var $isMain = false;
    var $routes;
    var $associatedSubnets;
    
    public function __construct($id) {
        $this->id = $id;
        $this->routes = array();
        $this->associatedSubnets = array();
    }
    
    public function setVpc($vpc) {
        $this->vpc = $vpc;
    }
    
    public function setMain(bool $isMain) {
        $this->isMain = $isMain;
        if($isMain) {
            $this->vpc->setMainRouteTable($this);
        }
    }
    
    public function getVpc() {
        return $this->vpc;
    }

    public function addAssociatedSubnet(MySubnet $sn) {
        $this->associatedSubnets[$sn->getId()] = $sn;
        $sn->setRouteTable($this);
    }
    
    public function getAssociatedSubnets() {
        return $this->associatedSubnets;
    }
        
    public function addRoute(MyRoute $r) {
        $this->routes[$r->getId()] = $r;
    }
    
    public function getRoutes() {
        return $this->routes;
    }
        
}