<tr>
    <td><i class="{{ $icon }}"></i> {{ $IpPermissionName }}</td>
    <td class="text-center @if(@$perm['FromPort'] == '') text-muted @endif">
        @if(@$perm['IpProtocol'] == '-1')
            (ALL)
        @else
            {{ strtoupper($perm['IpProtocol']) }}
        @endif
    </td>
    <td class="text-center @if(@$perm['FromPort'] == '') text-muted @endif">
        @if(@$perm['FromPort'] != '')
            @if(@$perm['FromPort'] == @$perm['ToPort'])
            {{ $perm['FromPort'] }}
            @else
            {{ $perm['FromPort'] or ""}} - {{ $perm['ToPort'] or ""}}
            @endif
        @else
        (ALL)
        @endif
    </td>
    <td>
        {{-- IP Address --}}
        @foreach($perm['IpRanges'] as $ir)
            <div><i class="{{ $icon }}"></i> {{ $ir['CidrIp'] }}</div>
        @endforeach

        {{-- Security Group --}}
        @foreach($perm['UserIdGroupPairs'] as $ug)
        <div>
            <p>
                <i class="{{ $icon }}"></i> <a href="#{{ $ug['GroupId'] }}" class="label label-info">{{ $ug['GroupId'] }}</a>
                @if($sg->getId() == $ug['GroupId'])
                <span> (itself) {{ @$aws->getSecurityGroup($ug['GroupId'])->getName() }}</span>
                @else
                <a href="#{{ $ug['GroupId'] }}">{{ @$aws->getSecurityGroup($ug['GroupId'])->getName() }}</a>
                <div class="col-md-12">
                    <table class="table table-bordered table-condensed">
                        <tr class="active">
                            <th class="text-nowrap col-md-2">Type</th>
                            <th class="text-nowrap col-md-10">Name</th>
                        </tr>
                        @foreach($aws->getSecurityGroup($ug['GroupId'])->getInstances() as $sgm)
                        <tr>
                            <td class="text-nowrap">Instance</td>
                            <td class="text-nowrap"><a href="#{{ $sgm->getId() }}"><strong>{{ $sgm->getName() }}</strong></a></td>
                        </tr>
                        @endforeach
                        @foreach($aws->getSecurityGroup($ug['GroupId'])->getLoadBalancers() as $sgm)
                        <tr>
                            <td class="text-nowrap">ELB</td>
                            <td class="text-nowrap"><a href="#{{ $sgm->getId() }}"><strong>{{ $sgm->getName() }}</strong></a></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                @endif
            </p>
        </div>
        @endforeach
    </td>
</tr>