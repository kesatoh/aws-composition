<?php
namespace App\Models\MyAWS;

use App\Models\MyAWS\MyAwsObject;

class MyInternetGateway extends MyAwsObject {
    var $vpc;
    
    public function __construct($id) {
        $this->id = $id;
    }
    
    public function setVpc($vpc) {
        $this->vpc = $vpc;
    }
    
    public function getVpc() {
        return $this->vpc;
    }

}