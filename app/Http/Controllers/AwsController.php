<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\MyAWS\MyAwsFactory;
use App\Models\MyAWS\MyAws;
use App\Models\MyAWS\MyVpc;
use App\Models\MyAWS\MySubnet;
use App\Models\MyAWS\MyEC2Instance;

class AwsController extends Controller
{
    //
    function listAll(Request $req) {
        $pVpcId = $req->input('vpcId');

        $aws = MyAwsFactory::create();
        $vpcs = array();        
        if($pVpcId) {
            $vpcs[$pVpcId] = $aws->getVpc($pVpcId);
        }
        else {
            $vpcs = $aws->getVpcs();
        }
        $vpcs = $aws->getVpcs();

        return view('aws', compact("aws", "vpcs"));
    }

    
    //
    function ddvpc() {
        $ec2 = \AWS::createClient('ec2');
        // Get AZ information
        $result = $ec2->describeVpcs();
        dump_r($result);
    }

    //
    function ddaz() {
        $ec2 = \AWS::createClient('ec2');
        // Get AZ information
        $result = $ec2->describeAvailabilityZones();
        dump_r($result);
    }

    //
    function ddsn() {
        $ec2 = \AWS::createClient('ec2');
        // Get Subnet information
        $result = $ec2->describeSubnets();
        dump_r($result);
    }

    //
    function ddsg() {
        $ec2 = \AWS::createClient('ec2');
        // Get Security Group information
        $sgsResult = $ec2->describeSecurityGroups();
        $sgs = $sgsResult['SecurityGroups'];
        dump_r($sgs);
    }

    //
    function ddec2() {
        $ec2 = \AWS::createClient('ec2');
        // Get Security Group information
        $result = $ec2->describeInstances();
        $ins = $result['Reservations'];
        dump_r($ins);
    }

    //
    function ddelb() {
        $elbClient = \AWS::createClient('elasticloadbalancing');
        $elbsResult = $elbClient->describeLoadBalancers();
        $allElbs = $elbsResult['LoadBalancerDescriptions'];
        dump_r($allElbs);
    }

    //
    function ddrds() {
        $ec2 = \AWS::createClient('rds');
        // Get RDS information
        $result = $ec2->describeDBInstances();
        $ins = $result['DBInstances'];
        dump_r($ins);
    }

    //
    function ddebtapp() {
        $client = \AWS::createClient('ElasticBeanstalk');
        // Get Elastic Beanstalk information
        // $result = $client->describeEnvironments();
        $result = $client->describeApplications();
        dump_r($result);
    }

    //
    function ddebtenv() {
        $client = \AWS::createClient('ElasticBeanstalk');
        // Get Elastic Beanstalk information
        // $result = $client->describeEnvironments();
        $envs = $client->describeEnvironments();
        foreach($envs['Environments'] as $env) {
            dump_r($env);
            $result = $client->describeEnvironmentResources(['EnvironmentId' => $env['EnvironmentId']]);
            dump_r($result);
        }
    }

    //
    function ddrt() {
        $client = \AWS::createClient('ec2');
        // Get Elastic Beanstalk information
        $result = $client->describeRouteTables();
        dump_r($result);
    }

    //
    function ddnatgw() {
        $client = \AWS::createClient('ec2');
        // Get NAT Gateway information
        $result = $client->describeNatGateways();
        dump_r($result);
    }

    //
    function ddigw() {
        $client = \AWS::createClient('ec2');
        // Get NAT Gateway information
        $result = $client->describeInternetGateways();
        dump_r($result);
    }

    //
    function ddeni() {
        $client = \AWS::createClient('ec2');
        // Get NAT Gateway information
        $result = $client->describeNetworkInterfaces();
        dump_r($result);
    }

}
