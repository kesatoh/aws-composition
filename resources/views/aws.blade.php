<!DOCTYPE html>
<html>
    <head>
        <title>AWS Information</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <style>
            body { background-image: url(aws/img/blur-02.jpg); background-attachment: fixed }
            div.container { background: rgba(255, 255, 255, 0.3); box-shadow: 0px 0px 20px rgba(100, 100, 100, 0.9); }
            div[data-toggle=collapse] { cursor: pointer; }
            table { background: white; }
            fieldset { margin: 5px 0px; padding: 5px 20px; border: 1px solid gray; border-radius: 5px; }
            legend { margin: 0; padding: 0 10px; border: 0; width: inherit; font-size: 18px; }
            ul { padding: 0; }
            li { list-style: none; }
            .legend-strong { border: 1px solid red; background-color: rgba(255, 255, 255, 0.75); }
            .ellipse { overflow: hidden; white-space: nowrap; text-overflow: ellipsis; }
            .icon-green:before { content: " "; width: 32px; height: 32px; background-image: url(aws/img/bullet-green.png); display: inline-block; vertical-align: middle; }
            .icon-red:before { content: " "; width: 32px; height: 32px; background-image: url(aws/img/bullet-red.png); display: inline-block; vertical-align: middle; }
            .icon-overlay { position: relative; display: inline-block; }
            .icon-green-overlay { content: ""; position: absolute; top: -16px; left: -16px; width: 32px; height: 32px; background: url("aws/img/bullet-green.png") top right no-repeat; }
            .icon-red-overlay { content: ""; position: absolute; top: -16px; left: -16px; width: 32px; height: 32px; background: url("aws/img/bullet-red.png") top right no-repeat; }
        </style>
        <script type="text/javascript">
        $(function() {
            $(document).on("click", "a", function() {
                var hashTabName = document.location.hash;
                console.log(hashTabName);
                if (hashTabName) {
                    // #(ハッシュ)指定されたタブを表示する
                    $('a[href='
                            + hashTabName + ']').tab('show');

                    // 所定の位置までスクロールする
                    var tabParent = $("#" + $('.tab-pane a[href=' + hashTabName + ']').parents('div').attr('id'));
                    $('html, body').stop().animate({
                        scrollTop: tabParent.offset().top
                    }, 500);
                }
            });
        });
        </script>
    </head>
    <body>
        <div class="container">

            <h1>AWS Environment</h1>
{{--
            <p>
                {{ Form::open([ 'method' => 'get' ]) }}
                <div class="form-group">
                    <label>Select VPC</label>
                    <select name="vpcId" class="form-control">
                        @foreach($aws->getVpcs() as $vpcId => $vpc)
                        <option value="{{ $vpcId }}">{{ $vpcId }} ({{ $vpc->getName() }})</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                {{ Form::close() }}
            </p>

            <hr>
--}}
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#composition">Composition</a></li>
                <li><a data-toggle="tab" href="#vpc">VPC ({{ count($aws->getVpcs()) }})</a></li>
                <li><a data-toggle="tab" href="#subnet">Subnet ({{ count($aws->getSubnets()) }})</a></li>
                <li><a data-toggle="tab" href="#security_group">Security Group ({{ count($aws->getSecurityGroups()) }})</a></li>
                <li><a data-toggle="tab" href="#ec2_instance">EC2 Instance ({{ count($aws->getInstances()) }})</a></li>
                <li><a data-toggle="tab" href="#load_balancer">Load Balancer ({{ count($aws->getLoadBalancers()) }})</a></li>
                <li><a data-toggle="tab" href="#rds_database">RDS Databse ({{ count($aws->getRDSs()) }})</a></li>
            </ul>
            
            <div class="tab-content">
            @include('aws.aws-composition')
            @include('aws.aws-vpc')
            @include('aws.aws-subnet')
            @include('aws.aws-securitygroup')
            @include('aws.aws-ec2instance')
            @include('aws.aws-elb')
            @include('aws.aws-rds')
            </div>
            
        </div>
    </body>
</html>
