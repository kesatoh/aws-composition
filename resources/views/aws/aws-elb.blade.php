<div id="load_balancer" class="tab-pane fade">
    <div class="content">
        <table class="table table-bordered">
            <tr class="active">
                <th>Balancer Name</td>
                <th>VPC ID</td>
                <th>VPC Name</td>
            </tr>
            @foreach($aws->getLoadBalancers() as $elb)
            <tr>
                <td class="text-nowrap"><strong>{{ $elb->getName() }}</strong></td>
                <td class="text-nowrap"><span class="label label-danger">{{ $elb->getVpc()->getId() }}</span></td>
                <td class="text-nowrap">{{ $elb->getVpc()->getName() }}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>