<?php
namespace App\Models\MyAWS;

use App\Models\MyAWS\MyAwsObject;

class MyRoute extends MyAwsObject {
    var $vpc;
    var $routeTable;
    var $type;
    
    public function __construct($id) {
        $this->id = $id;
    }
    
    public function setVpc($vpc) {
        $this->vpc = $vpc;
    }
    
    public function getVpc() {
        return $this->vpc;
    }

    public function setRouteTable(MyRouteTable $rt) {
        $this->routeTable = $rt;
    }
        
    public function getRouteTable() {
        return $this->routeTable;
    }
        
    public function setRouteType($type) {
        $this->type = $type;
    }
        
    public function getRouteType() {
        return $this->type;
    }
        
}