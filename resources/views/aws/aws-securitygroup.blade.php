<div id="security_group" class="tab-pane fade">
    @foreach($aws->getSecurityGroups() as $sg)
    <div class="panel panel-default">
        
        <div id="{{ $sg->getId() }}" class="panel-heading" data-toggle="collapse" href="#{{ $sg->getId() }}-body">
            <span class="label label-info">{{ $sg->getId() }}</span> <strong>"{{ $sg->getName() }}"</strong>
            <span> - {{ $sg->get('Description') }}</span>
        </div>
        
        <div id="{{ $sg->getId() }}-body" class="panel-body panel-collapse collapse">
            <p class="col-md-12">
                <span class="label label-danger">{{ $sg->getVpc()->getId() }}</span> <span>"{{ $sg->getVpc()->getName() }}"</span>
            </p>

            <div class="table-responsive col-md-12">
                @include('aws.sg.aws-sgdetail')
            </div>

{{--
            <div class="col-md-12">
            @if(@$sg->getSecurityGroups())
                <table class="table table-bordered table-condensed">
                    <tr class="active">
                        <th class="col-md-2">Component</th>
                        <th class="col-md-2">ID</th>
                        <th class="col-md-8">Name</th>
                    </tr>
                    @foreach($sgMap[$sg['GroupId']] as $sgm)
                    <tr>
                        <td class="text-nowrap">{{ $sgm['ComponentName'] }}</td>
                        <td class="text-nowrap">{{ $sgm['Id'] }}</td>
                        <td class="text-nowrap"><a href="#{{ $sgm['Id'] }}"><strong>{{ $sgm['Name'] }}</strong></a></td>
                    </tr>
                    @endforeach
                </table>
            @else
            <span class="alert alert-danger glyphicon glyphicon-exclamation-sign" role="alert"> <strong>NO ASSIGNED COMPONENTS.</strong> This Security Group might not be required?</span>
            @endif
            </div>
--}}
        </div>
    </div>
    @endforeach
</div>