<?php
namespace App\Models\MyAWS;

use App\Models\MyAWS\MyAwsObject;

class MyNatGateway extends MyAwsObject {
    var $subnet;
    
    public function __construct($id) {
        $this->id = $id;
    }
    
    public function setSubnet($sn) {
        $this->subnet = $sn;
    }
    
    public function getSubnet() {
        return $this->subnet;
    }

}